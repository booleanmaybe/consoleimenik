-- -----------------------------------------------------
-- Schema imenik
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `imenik` DEFAULT CHARACTER SET latin1 ;
USE `imenik` ;

-- -----------------------------------------------------
-- Table `imenik`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `imenik`.`user` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `dateRegistered` DATETIME NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  PRIMARY KEY (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `imenik`.`contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `imenik`.`contact` (
  `contactId` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `phone` VARCHAR(20) NULL DEFAULT NULL,
  `note` VARCHAR(250) NULL DEFAULT NULL,
  `dateAdded` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`contactId`),
  INDEX `user.username foreign key` (`username` ASC),
  CONSTRAINT `user.username`
    FOREIGN KEY (`username`)
    REFERENCES `imenik`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
