package org.bildit.consoleImenik.bo.tests;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bildit.consoleImenik.bo.ContactBOImplementation;
import org.bildit.consoleImenik.dao.ContactDAO;
import org.bildit.consoleImenik.dto.Contact;
import org.junit.Test;
import org.mockito.Mockito;

public class ContactBOImplementationTest {

	ContactDAO mockDao;
	ContactBOImplementation bo;

	@Test
	public void shouldReturnListOfUsersWhenCalled() throws SQLException {

		mockDao = Mockito.mock(ContactDAO.class);
		bo = new ContactBOImplementation();
		bo.setDao(mockDao);

		Contact contact = new Contact();

		ArrayList<Contact> list = new ArrayList<>();
		list.add(contact);

		Mockito.when(mockDao.getAllContacts("test")).thenReturn(list);

		ArrayList<Contact> result = bo.getAllContactsDao("test");
		assertSame(list.get(0), result.get(0));
		
	}
}
