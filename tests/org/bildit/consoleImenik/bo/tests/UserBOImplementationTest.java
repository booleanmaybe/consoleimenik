package org.bildit.consoleImenik.bo.tests;

import static org.junit.Assert.*;

import java.sql.SQLException;
import org.bildit.consoleImenik.bo.UserBOImplementation;
import org.bildit.consoleImenik.dao.UserDAO;
import org.bildit.consoleImenik.dto.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class UserBOImplementationTest {

	UserBOImplementation bo;
	UserDAO mockDao;
	User emptyUser;
	User validUser;

	@Before
	public void setUp() {

		bo = new UserBOImplementation();
		mockDao = Mockito.mock(UserDAO.class);
		bo.setDao(mockDao);

		emptyUser = new User("", "");
		validUser = new User("test", "password");
	}

	@Test
	public void shouldReturnFalseWhenUserIsNull() throws SQLException {

		boolean result = bo.registerUserBo(null);
		assertFalse(result);

	}

	@Test
	public void registerShouldReturnFalseWhenUsernameIsAnEmptyString() throws SQLException {

		boolean result = bo.registerUserBo(emptyUser);
		assertFalse(result);

	}

	@Test
	public void registerShouldReturnFalseWhenPasswordIsAnEmptyString() throws SQLException {

		boolean result = bo.registerUserBo(emptyUser);
		assertFalse(result);

	}

	@Test
	public void registerShouldReturnTrueWhenDAOMethodIsCalled() throws SQLException {

		// expectation stub
		Mockito.when(mockDao.registerUser(validUser)).thenReturn(true);

		boolean result = bo.registerUserBo(validUser);
		assertTrue(result);
		// verifikacija da li je metoda pozvana
		Mockito.verify(mockDao).registerUser(validUser);

	}

	@Test
	public void loginShouldReturnFalseWhenUsernameIsAnEmptyString() throws SQLException {

		boolean result = bo.userLoginBo(emptyUser.getUsername(), emptyUser.getPassword());
		assertFalse(result);

	}

	@Test
	public void loginShouldReturnFalseWhenPasswordIsAnEmptyString() throws SQLException {

		boolean result = bo.userLoginBo(emptyUser.getUsername(), emptyUser.getPassword());
		assertFalse(result);

	}

	@Test
	public void loginShouldReturnFalseIfPasswordsDontMatch() throws SQLException {

		// expectation stub
		Mockito.when(mockDao.getUser(validUser.getUsername())).thenReturn(validUser);
		boolean result = bo.userLoginBo(validUser.getUsername(), "asd");
		assertFalse(result);

		// verifikacija da li je metoda pozvana
		Mockito.verify(mockDao).getUser(validUser.getUsername());
	}
	
	@Test
	public void loginShouldReturnTrueWhenPasswordsMatch() throws SQLException{
		
		// expectation stub
		Mockito.when(mockDao.getUser(validUser.getUsername())).thenReturn(validUser);
		boolean result = bo.userLoginBo(validUser.getUsername(), validUser.getPassword());
		assertTrue(result);

		// verifikacija da li je metoda pozvana
		Mockito.verify(mockDao).getUser(validUser.getUsername());
	}
	
}