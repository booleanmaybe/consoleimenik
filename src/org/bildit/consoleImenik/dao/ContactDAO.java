package org.bildit.consoleImenik.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import org.bildit.consoleImenik.dto.Contact;

public interface ContactDAO {

	public ArrayList<Contact> getAllContacts(String username) throws SQLException;

	public Contact getContact(int contactId) throws SQLException;

	public boolean addContact(Contact contact) throws SQLException;
	
	public boolean editContact(Contact contact) throws SQLException;

	public boolean deleteContact(int contactId) throws SQLException;

}
