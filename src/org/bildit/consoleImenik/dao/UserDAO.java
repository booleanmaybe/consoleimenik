package org.bildit.consoleImenik.dao;

import java.sql.SQLException;
import org.bildit.consoleImenik.dto.User;

public interface UserDAO {

	public boolean registerUser(User user) throws SQLException;

	public User getUser(String username) throws SQLException;

}