package org.bildit.consoleImenik.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bildit.consoleImenik.dto.User;

public class UserDAOImplementation implements UserDAO {

	Connection connection = ConnectionManager.getInstance().getConnection();

	@Override
	public boolean registerUser(User user) throws SQLException {

		String query = "INSERT INTO user(username, password, dateRegistered, isActive) VALUES(?,?,?,?)";

		try (PreparedStatement statement = connection.prepareStatement(query);) {

			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			statement.setTimestamp(3, user.getDateRegistered());
			statement.setBoolean(4, user.isActive());

			statement.executeUpdate();

			return true;
		}
	}

	@Override
	public User getUser(String username) throws SQLException {
		
		User user = null;
		String query = "SELECT * FROM user WHERE username = ?";
		ResultSet rs = null;

		try (PreparedStatement statement = connection.prepareStatement(query);) {

			statement.setString(1, username);
			rs = statement.executeQuery();
			rs.next();

			user = new User(
					rs.getString("username"), 
					rs.getString("password"), 
					rs.getTimestamp("dateRegistered"),
					rs.getBoolean("isActive")
					);

			rs.close();
			
			return user;
		}
	}

}