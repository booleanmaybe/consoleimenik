package org.bildit.consoleImenik.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bildit.consoleImenik.dto.Contact;

public class ContactDAOImplementation implements ContactDAO {

	Connection connection = ConnectionManager.getInstance().getConnection();

	@Override
	public ArrayList<Contact> getAllContacts(String username) throws SQLException {

		ArrayList<Contact> contacts = new ArrayList<>();
		String query = "SELECT * FROM contact WHERE username = ?";
		ResultSet rs = null;

		try (PreparedStatement statement = connection.prepareStatement(query);) {

			statement.setString(1, username);
			rs = statement.executeQuery();
			
			while(rs.next()){
				
				contacts.add(
						new Contact(
								rs.getInt("contactId"),
								rs.getString("username"),
								rs.getString("name"),
								rs.getString("lastname"),
								rs.getString("email"),
								rs.getString("phone"),
								rs.getString("note"),
								rs.getTimestamp("dateAdded")
								)
						);
			}
			
			return contacts;
		}
	}

	@Override
	public Contact getContact(int contactId) throws SQLException {

		Contact contact = null;
		String query = "SELECT * FROM contact WHERE contactId = ?";
		ResultSet rs = null;
		
		try (PreparedStatement statement = connection.prepareStatement(query);) {
			
			statement.setInt(1, contactId);
			rs = statement.executeQuery();
			rs.next();
			
			contact = new Contact(
					rs.getInt("contactId"),
					rs.getString("username"),
					rs.getString("name"),
					rs.getString("lastname"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("note"),
					rs.getTimestamp("dateAdded")
					);
			
			return contact;
		}
	}

	@Override
	public boolean addContact(Contact contact) throws SQLException {

		String query = "INSERT INTO contact(username, name, lastname, email, phone, note, dateAdded)"
				+ " VALUES(?,?,?,?,?,?,?)";
		
		try (PreparedStatement statement = connection.prepareStatement(query);) {
			
			statement.setString(1, contact.getUsername());
			statement.setString(2, contact.getName());
			statement.setString(3, contact.getLastName());
			statement.setString(4, contact.getEmail());
			statement.setString(5, contact.getPhone());
			statement.setString(6, contact.getNote());
			statement.setTimestamp(7, contact.getDateAdded());
			
			statement.executeUpdate();
			
			return true;
		}
	}

	@Override
	public boolean editContact(Contact contact) throws SQLException {

		String query = "UPDATE contact SET name = ?, lastname = ?, email = ?, phone = ?, note = ? WHERE contactId = ?";
		
		try (PreparedStatement statement = connection.prepareStatement(query);) {

			statement.setString(1, contact.getName());
			statement.setString(2, contact.getLastName());
			statement.setString(3, contact.getEmail());
			statement.setString(4, contact.getPhone());
			statement.setString(5, contact.getNote());
			statement.setInt(6, contact.getContactId());
			
			statement.executeUpdate();
			
			return true;
		}
	}

	@Override
	public boolean deleteContact(int contactId) throws SQLException {

		String query = "DELETE FROM contact WHERE contactId = ?";
		
		try (PreparedStatement statement = connection.prepareStatement(query);) {
			
			statement.setInt(1, contactId);
			
			statement.executeUpdate();
			
			return true;
		}		
	}
	
}