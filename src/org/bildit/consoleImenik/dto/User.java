package org.bildit.consoleImenik.dto;

import java.sql.Timestamp;
import java.util.Date;

public class User {

	private String username;
	private String password;
	private Timestamp dateRegistered;
	private boolean isActive;

	// konstruktor za kada spremamo u bazu
	public User(String username, String password) {

		// datum i vrijeme u ispravnom mysql formatu
		Date date = new Date();
		Timestamp mysqlDate = new Timestamp(date.getTime());

		this.username = username;
		this.password = password;
		this.dateRegistered = mysqlDate;
		this.isActive = true;
	}

	// konstruktor za kada povlacimo iz baze
	public User(String username, String password, Timestamp dateRegistered, boolean isActive) {
		this.username = username;
		this.password = password;
		this.dateRegistered = dateRegistered;
		this.isActive = isActive;
	}

	public User() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getDateRegistered() {
		return dateRegistered;
	}

	public void setDateRegistered(Timestamp dateRegistered) {
		this.dateRegistered = dateRegistered;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		this.isActive = active;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", dateRegistered=" + dateRegistered + ", active="
				+ isActive + "]";
	}
}
