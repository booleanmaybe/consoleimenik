package org.bildit.consoleImenik.dto;

import java.sql.Timestamp;
import java.util.Date;

public class Contact {

	private int contactId;
	private String username;
	private String name;
	private String lastName;
	private String email;
	private String phone;
	private String note;
	private Timestamp dateAdded;

	// konstruktor za kada spremamo u bazu
	public Contact(String username, String name, String lastName, String email, String phone, String note) {

		// datum i vrijeme u ispravnom mysql formatu
		Date date = new Date();
		Timestamp mysqlDate = new Timestamp(date.getTime());

		this.username = username;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.note = note;
		this.dateAdded = mysqlDate;
	}

	// konstruktor za kada povlacimo iz baze
	public Contact(int contactID, String username, String name, String lastName, String email, String phone,
			String note, Timestamp dateAdded) {
		this.username = username;
		this.contactId = contactID;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.note = note;
		this.dateAdded = dateAdded;
	}

	public Contact() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactID) {
		this.contactId = contactID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Timestamp getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	@Override
	public String toString() {
		return "Contact [username=" + username + ", contactID=" + contactId + ", name=" + name + ", lastName=" + lastName
				+ ", email=" + email + ", phone=" + phone + ", note=" + note + ", dateAdded=" + dateAdded + "]";
	}
}
