package org.bildit.consoleImenik.bo;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bildit.consoleImenik.dao.ContactDAO;
import org.bildit.consoleImenik.dto.Contact;

public class ContactBOImplementation implements ContactBO {

	private ContactDAO dao = null;

	@Override
	public ArrayList<Contact> getAllContactsDao(String username) throws SQLException {

		ArrayList<Contact> result = dao.getAllContacts(username);
		return result;
	}

	@Override
	public Contact getContactBo(int contactId) throws SQLException {

		return null;
	}

	@Override
	public boolean addContactBo(Contact contact) throws SQLException {

		return false;
	}

	@Override
	public boolean editContactBo(int contactId) throws SQLException {

		return false;

	}

	@Override
	public boolean deleteContactBo(int contactId) throws SQLException {

		return false;

	}

	public void setDao(ContactDAO dao) {
		this.dao = dao;
	}

}
