package org.bildit.consoleImenik.bo;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bildit.consoleImenik.dto.Contact;

public interface ContactBO {

	public ArrayList<Contact> getAllContactsDao(String username) throws SQLException;

	public Contact getContactBo(int contactId) throws SQLException;

	public boolean addContactBo(Contact contact) throws SQLException;
	
	public boolean editContactBo(int contactId) throws SQLException;

	public boolean deleteContactBo(int contactId) throws SQLException;
	
}
