package org.bildit.consoleImenik.bo;

import java.sql.SQLException;

import org.bildit.consoleImenik.dto.User;

public interface UserBO {

	public boolean registerUserBo(User user) throws SQLException;
	
	public boolean userLoginBo(String username, String password) throws SQLException;

}