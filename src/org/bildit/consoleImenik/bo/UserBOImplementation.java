package org.bildit.consoleImenik.bo;

import java.sql.SQLException;

import org.bildit.consoleImenik.dao.UserDAO;
import org.bildit.consoleImenik.dto.User;

public class UserBOImplementation implements UserBO {

	private UserDAO dao = null;
	private User user = null;

	@Override
	public boolean registerUserBo(User user) throws SQLException {

		if (user == null) {

			return false;

		} else {

			if (user.getUsername() == "" || user.getPassword() == "") {

				return false;

			} else {

				return dao.registerUser(user);

			}
		}
	}

	@Override
	public boolean userLoginBo(String username, String password) throws SQLException {

		if (username == "" || password == "") {

			return false;

		} else {

			User user = dao.getUser(username);

			if (user.getPassword() != password) {

				return false;

			} else {

				this.user = user;
				return true;
			}
		}
	}

	public void setDao(UserDAO dao) {
		this.dao = dao;
	}

	public User getUser() {
		return user;
	}

}